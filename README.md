# Javascript på Svenska

## Vad är ett program

Ett program är en lista med instruktioner som en dator går igenom för att göra olika saker.

## Ditt första program

Om du använder dig av Chrome så har du faktiskt en möjlighet att testa javascript direkt i webbläsaren. Börja med att trycka på F12 för att få fram developer tools, vilket är den verktygslåda som utveckare använder för att se vad som händer på en webbplats. Developer tools har väldigt många funktioner, men just nu är vi intresserade av att testa javascipt - så klicka på fliken "console" och skriv in följande.

```javascript
console.log("Hej!");
```

Grattis! - du har just skrivit ditt första program. Tryck enter för att köra programmet och notera vad som skrivs ut. Som du kanske märker så gör inte programmet så mycket mer än att skriver ut "hej". För att göra detta har vi använt en funktion som heter `console.log()`, vilket är en inbyggd funktion i JavaScript och används för att skriva ut saker.

```javascript
console.log(123); // Skriver ut 123
console.log(123, "Hej"); // Skriver ut 123Hej
```

## Variabler och konstanter

För att ett program skall kunna komma ihåg saker så använder man sig av variabler och konstanter. Man kan se dessa som lådor med ett namn på, i vilka du kan stoppa vad som helst.

### Variabler

För att skapa en variabel (låda) som innehåller antalet kronor vi har gör man följande.

```javascript
var antalKronor = 12;
```

Man börjar alltså raden med `var`, vilket betyder 'variabel'. Vi går sedan vidare och ger vår variabel ett namn genom att skriva `antalKronor`. Vi kan sedan avsluta det hela genom att skriva `= 12` vilket betyder att vi vill stoppa in 12 i variabeln.

Vi kan även när som helst välja att stoppa någonting annat i variabeln. I exemplet nedan så skapar vi först en variabel som innehåller 12, för att sedan byta ut innehållet i samma variabel till 100.

```javascript
var antalKronor = 12;
antalKronor = 100; // Vår variabel innehåller nu 100
```

I den senaste versionen av javascript kan man även använda sig av nyckelordet `let` för att skapa en variabel. De skillader som finns mellan `let` och `var` kommer vi (kanske) att gå igenom senare, men just nu kan de använda dom på samma sätt.

```javascript
let antalKronorMedLet = 12;
var antalKronorMedVar = 12;
```

### Konstanter

En konstant är en låda som man _inte kan ändra på_ och fungerar i övrigt som en variabel. Istället för att använda oss av `var` så använder vi istället nyckelordet `const`. Om vi provar att som i fallet ovan ersätta 12 med 100 så går det helt enkelt inte.

```javascript
const antalKronor = 12;
antalKronor = 100; // Här blir det fel i programmet
```

### Vad kan variabler och konstanter innehålla?

Variabler och konstanter kan innehålla i princip vad som helst. Vanligast är att dom innehåller nummer och text, men dom kan även innehålla funktioner och andra mer komplicerade objekt.

```javascript
var antalKronor = 12;
var antalKronorText = "tolv";
```

### Att skriva ut innehållet i variabler och konstanter

För att skriva ut saker så använder du samma funktion som du använde för att skriva ut "Hej!" tidigare. Du kan skriva ut både variabler och konstanter.

```javascript
var antalKronor = 12;
console.log(antalKronor); // skriver ut '12'
var antalKronorText = "tolv";
console.log(antalKronorText); // skriver ut 'tolv'
```

## Matematiska operatorer

För att förändra värdet i en variabel så kan man man använda så kallade _matematiska operatorer_, eller "plus och minus" på ren svenska.

### Plus och minus

För att addera två tal med varandra så använder vi oss av `+`. Vi kan addera tal direkt samt tal som finns i variabler och konstanter.

```javascript
console.log(5 + 5); // 10

var fem = 5;
console.log(fem + 5); // 10

const tio = 10;
console.log(5 + tio); // 15
```

Vill man addera ett värde till en variabel så kan man göra på följande sätt.

```javascript
var talet = 10;

talet = talet + 1; // 11

talet += 1; // 12

talet++; // 13
```

Alla tre sätt gör samma sak, nämligen ökar talet i vår variabel med ett. På samma sätt kan vi subtrahera.

```javascript
var talet = 10;

talet = talet - 1; // 9

talet -= 1; // 8

talet--; // 7
```

### Gånger och delat

Inte helt oväntat så fungerar gånger och delat på samma sätt som addition och subtraktion.

```javascript
var talet = 10;

talet = talet * 2; // 20

talet *= 2; // 40

talet = talet / 2; // 20

talet /= 2; // 10
```

### Modulo %

En operation som ofta används inom programmering är modulo. Modulo representeras av `%`och kan används för att få ut den så kallade resten.

Säg att vi vi har en tårta med 10 färdigskurna bitar. Vi skall dela tårtan mellan 4 personer och för att räkna ut vad som blir över om alla skall få lika många hela bitar kan vi använda modulo på följande sätt.

```javascript
const antalBitar = 10;
const antalPersoner = 4;
const antalBitarOver = antalBitar % antalPersoner; // resten är 2, det blir alltså två bitar över.
```

Om man skulle vilja beräkna modulo manuellt så kan man använda formeln `a % b = a - floor(a / b) * b`, där `floor` används för att avrunda ned till närmaste heltal. I vårt fall med tårtbitarna så ser formeln ut på följande sätt

`antalBitar % antalPersoner = antalBitar - floor(antalBitar / antalPersoner) * antalPersoner`

Det finns dock andra mer intressanta användningsområden för modulo, men det kan vi ta lite senare. Låt oss istället se ett par exempel för att bli varma i kläderna.

```javascript
console.log(25 % 10); // 5 - eftersom vi får in 10 två gånger i 25 vilket blir 20, 5 blir över.

console.log(25 % 5); // 0 - eftersom vi kan in 5 5 gånger i 25, ingenting blr över.

console.log(25 % 1); // 0 - vi får in 25 ettor i 25, ingenting blir över.

console.log(10 % 4); // 2 - vi får in en fyra två gånger i 10 vilket blir åtta. Två blir över.
```

## If-satser

Ett program med enbart variabler kan inte göra så mycket mer än att skriva ut dom på skärmen. Om vi istället för att skriva ut allting rakt av vill styra när och varför någonting skrivs ut så kan vi använda oss av if-satser.

```javascript
var antalKronor = 12;
if (antalKronor === 12) {
  console.log("Du har tolv kronor");
}
```

Vi börjar programmet med att skapa en variabel som innehåller talet 12. Vi går sedan vidare med att använda `if` för att visa att det är just en if-sats som vi vill använda oss av. Inom parenteserna skriver vi sedan `(antalKronor === 12)` vilket är uttrycket som if-satsen kollar om det är sant eller inte. Om uttrycket `(antalKronor === 12)` är sant så kommer vi att gå in i if-satsen och köra den kod som finns innanför måsvingarna, `{` `}`.

För att utvärdera en if-sats så använder vi följande uttryck.

```javascript
if (2 > 1) {
  // Om två är STÖRRE ÄN ett är uttrycket sant och vi kör koden innanför måsvingarna.
}

if (10 < 25) {
  // Om tio är MINDRE ÄN tjugofem är uttrycket sant och vi kör koden innanför måsvingarna.
}

if ("hej" === "hej") {
  // Om "hej" är SAMMA SAK som "hej" är uttrycket sant och vi kör koden innanför måsvingarna.
}

if ("hej" !== "nej") {
  // Om "hej" är INTE SAMMA SAK som "nej" är uttrycket sant och vi kör koden innanför måsvingarna.
}
```

Genom att använda dessa uttryck så kan vi skapa ett program som skriver ut text beroende på vilken temperatur vi angett i en variabel.

```javascript
var temperaturen = 12;

if (temperaturen < 0) {
  console.log("Det är mindre än noll grader ute, brr!");
}

if (temperaturen > 0) {
  console.log("Det är mer än noll grader ute, hurra"); // Detta kommer att skrivas ut
}

if (temperaturen === 0) {
  console.log("Det är exakt noll grader ute");
}
```

Orsaken att vi skriver ut att det är mer än noll grader handlar om att if-satsen tittar på varje uttryck och kollar om någonting är sant. I det här fallet så var `temperaturen < 0` inte sant, vilket gör att programmet inte kör koden innanför måsvingarna. Programmet går sedan vidare och kollar på nästa if-sats `temperaturen > 0` vilket visar sig vara sant och vi kan då gå innanför måsvingarna och köra den kod som finns där.

### Att använda && och || för att skapa mer avancerade if-satser

Ibland händer det att vi vill utvärdera mer än ett uttryck. Hur skall vi göra om vi t.ex. vill skriva ut någonting beroende på om temperaturen är mindre än 10 grader och det regnar.

```javascript
var temperaturen = 5;
var detRegnar = true;

if (temperaturen < 10 && detRegnar) {
  console.log("Det är under 10 grader OCH det regnar");
}
```

Här binder vi alltså ihop två uttryck genom att använda oss av `&&`. På samma sätt som vi använt `&&` kan vi använda oss av `||` för att skriva ut någonting om en sak är sann ELLER en annan.

```javascript
var temperaturen = 15;
var detRegnar = true;

if (temperaturen < 10 || detRegnar) {
  console.log("Det är under 10 grader ELLER regnar");
}
```

### Effektivare if-satser med else och else if

I ett exempel ovan skriver vi ut text (eller inte) beroende på vilken temperatur vi angett samt om det regnar eller inte. Det hände dock att man vill skriva ut någonting enbart om en tidigare if-sats inte accepterats. Låt oss titta på två program där det ena använder sig av enbart if-satser medans det andra använder sig av `if else`.

```javascript
var temperaturen = 5;
var detRegnar = true;

// Ett program som bara använder if
if (temperaturen < 10 && detRegnar) {
  console.log("Det är under 10 grader OCH det regnar");
}
if (detRegnar) {
  console.log("Det regnar");
}

// Ett program som använder else if
if (temperaturen < 10 && detRegnar) {
  console.log("Det är under 10 grader OCH det regnar");
} else if (detRegnar) {
  console.log("Det regnar");
}
```

Om vi tittar på vad våra två program skriver ut så kan vi se att det första programmet skriver ut både "Det är under 10 grader OCH det regnar" samt "Det regnar", vilket känns lite väl övertydligt. Tittar vi istället på vårt andra program kan vi se att det enbart skriver ut "Det är under 10 grader OCH regnar" vilket känns mer passande. Orsaken till att vårt andra program inte skriver ut "Det regnar" beror på att vi använt oss av `if else` när vi utvärderar våra if-satser. Detta får till följd att om vårt första uttryck är sant så försöker den inte ens utvärdera det andra.

Slutligen kan vi titta på hur vi kan använda `else` för att skriva ut någonting om en if-sats inte utvärderats till sant.

```javascript
var detRegnar = false;

if (detRegnar) {
  console.log("Det regnar");
} else {
  console.log("Det regnar inte");
}
```

Kör vi programmet så kommer det att skriva ut "Det regnar inte" eftersom det första uttrycket inte utvärderats till sant.

## Loopar

Tänk att vi vill skriva ut alla siffror från 0 till 99. Ett enkelt men tidsödande sätt att göra det på skulle vara att på varje rad skriva `console.log(0);` ända tills vi nått `console.log(99)`. Det skulle fungera, men det finns ett effektivare sätt att göra det på - loopar.

För att skapa en loop som skriver ut 0-99 så kan vi skriva följande

```javascript
for (var i = 0; i < 100; i++) {
  console.log(i);
}
```

Vi börjar med att berätta att det är for-loop vi vill använda genom att skriva `for`. För att programmet skall hålla reda på hur många gånger vi kört vår loop så skapar vi en variabel genom att skriva `var i = 0`. Att vi skriver just `0` betyder att vi vill att den skall börja på noll då vi tänkt räkna 0 - 99. För att veta när vi skall sluta köra vår loop så skriver vi `i < 100` vilket betyder att så länge `i < 100` är sant så fortsätter vi ett varv till. Till sist så har vi `i++`, det betyder att vid varje varv så vill vi att vår variabel i skall öka med ett. På så sätt kommer den att öka med 1 ända tills uttrycket `i < 100` inte längre är sant och vår loop avbryts.

På samma sätt som vi räknar 0-99 kan vi räkna 99-0.

```javascript
for (var i = 99; i >= 0; i--) {
  console.log(i);
}
```

Vi börjar med `for` för att berätta att det är just en for-loop vi vill skapa. Vi går sedan vidare och sätter startvärdet på vår variabel till 99 genom att skriva `var i = 99`. Vi vet också att eftersom vi vill räkna från 99 ned till 0 så vill vi fortsätta loopen så länge vi inte nått just 0. Detta gör vi genom att skriva `i >= 0`, vilket betyder _"är i större ELLER lika med 0"_. Vi avslutar det hela genom att räkna nedåt istället för uppåt. Detta gör vi med `i--`, men vi skule även kunna skriva `i-=1` eller `i = i-1`.

### Modulo i loopar

Ett vanligt användningsområde för modulo är just i loopar. Lås oss säga att vi har en lista på 100 namn där vi fått till uppgift att göra listan mer läsbar genom att lägga in lite extra radbrytningar. Vår chef är övertygad om att vi bör skriva ut två tomma rader var 10'e rad - och med modulo kan vi lösa det hela enkelt och effektivt.

```javascript
for (var i = 0; i < 100; i++) {
  console.log(i);
  if (i % 10 === 0) {
    console.log("");
    console.log("");
  }
}
```

Låt oss titta lite närmare på `if(i % 10 === 0)`. Det if-satsen gör är att den kollar om vi har någon rest när vi använder `i % 10`, vilket på ren svenska betyder "kan vi få in ett antal hela tior inuti i utan att få någonting över". Om vi tar ett exempel där i är 18 så kan vi snabbt räkna ut att `18 % 10` inte är noll, då vi bara får in en tia i 18 och då får en rest på 8. Om vi däremot väntar lite tills i nått 20 så kan vi se att `20 % 10` är noll, då vi får in två hela 10 i 20 och därför inte heller får någon rest.

## Funktioner

Tänk att du skapat ett program som tar in ett antal värden och skriver ut dessa lite överallt i programmet. Du anropar `console.log("meddelandet")` på kanske 100 ställen och i det stora fungerar programmet bra. Du visar sedan programmet för din chef som säger _"ser bra ut, men kan du inte skriva ut vårt företagsnamn innan varje meddelande"_. För att lösa problemet måste du söka igenom programmet och skriva in företagsnamnet på 100 ställen i koden - låter inte speciellt kul eller hur?

Ett bra sätt att slippa liknande problem är att använda sig av funktioner. En funktion kan man se som en bit kod som gör en eller flera saker men som döljer detta för dig och istället ger dig ett enkelt gränssnitt. Vi har redan använt en funktion flera gånger, nämligen `console.log()`, en inbyggd funktion för att skriva ut text. För att skapa en egen funktion som gör samma sak så kan vi göra på följande sätt.

```javascript
const skrivUtText = function(texten) {
  console.log(texten);
};

skrivUtText("Detta är mitt meddelande!");
```

Det vi gör är att vi skapar en konstant (vi skulle även kunna använda oss av `var` eller `let`) som vi ger ett namn och sedan använder `=` för att säga att vi vill stoppa en funktion i konstanten (lådan). För att skapa själva funktionen använder vi oss av ordet `function` för att berätta för programmet att det är en funktion. Inom parenteser direkt efter funktionen berättar vi vad vi vill kunna skicka in. I det här fallet skrev vi `function(texten)` vilket betyder att när vi anropar vår funktion så kan vi skicka in en parameter och att parameterns namn är 'texten'. Vi kan sedan använda vår parameter på samma sätt som en variabel i koden innanför måsvingarna.

Tittar vi lite närmare på vad vår funktion egentligen gör så kan vi se att den skriver ut det vi skickar in på den genom att använda funktionen console.log(). Låt oss säga att ett halvår har gått och vi har skapat ett nytt program som gör ungefär samma sak som det tidigare, det skriver ut text lite här och var. Vi har dock använt oss av funktionen `skrivUtText()` istället för `console.log()`, så när chefen får ett infall och vill att vi skall skriva ut företagsnamnet i varje meddelande behöver vi bara göra följande ändring.

```javascript
const skrivUtText = function(texten) {
  console.log("Företag AB " + texten);
};

skrivUtText("Detta är mitt meddelande!");
```

Istället för att enbart skriva ut den text vi skickar in i vår funktion så kombinerar vi företagets namn med den text vi skickat in för att tillsist skriva ut meddelandet, men nu med företagsnamnet.

### Andra sätt att skapa funktioner

För att göra det hela lite krångligt så finns det såklart olika sätt att skapa funktioner. Låt oss titta på tre exempel av funktionen ovan som gör samma sak.

```javascript
const skrivUtTextA = function(texten) {
  console.log("Företag AB " + texten);
};

function skrivUtTextB(texten) {
  console.log("Företag AB " + texten);
}

// "Arrow function"
const skrivUtTextC = texten => {
  console.log("Företag AB " + texten);
};

skrivUtTextA("Detta är mitt meddelande!");
skrivUtTextB("Detta är mitt meddelande!");
skrivUtTextC("Detta är mitt meddelande!");
```

Det finns vissa skillnader mellan "arrow functions" och de andra sätten att definiera en funktion men i det stora hela fungerar dom på samma sätt.

### Parametrar

Som du sett innan så kan man skicka in en parameter i en funktion. En parameter är en variabel som sedan kan användas inuti funktionen. För att skicka in mer än en parameter använder vi oss av ett komma, i det här fallet för att kunna skicka in företagsnamet i vår funktion.

```javascript
const skrivUtText = function(texten, foretagsNamn) {
  console.log(foretagsNamn + " " + texten);
};

skrivUtText("Detta är mitt meddelande!", "Företag AB");
```

Om vi vill att vår metod skall kunna anropas både med och utan företagsnamn så kan vi använda `=` för att bestämma ett standardvärde som skall användas om inget annat värde angetts.

```javascript
const skrivUtText = function(texten, foretagsNamn = "Företag AB") {
  console.log(foretagsNamn + " " + texten);
};

skrivUtText("Meddelande"); // Skriver ut "Företag AB Meddelande"
skrivUtText("Meddelande", "Potatis AB"); // Skriver ut "Potatis AB Meddelande"
```

Vi kan även använda oss av if-satser inuti våra funktioner. Låt oss skapa en funktion som skriver ut hur vi skall klä oss om vi skickar in temperatur samt om det regnar.

```javascript
const vilkaKladerSkallJagHa = function(temperatur, nederbord) {
  if (temperatur > 0 && nederbord) {
    console.log("Regnjacka");
  } else if (temperatur < -5 && nederbord) {
    console.log("Snöskor och skidglasögon");
  } else if (temperatur < -5 && !nederbord) {
    console.log("Vinterjackan");
  } else {
    console.log("Varmare än noll och det regnar inte, ta en tjocktröja!");
  }
};

vilkaKladerSkallJagHa(-10, true); // "Snöskor.."
vilkaKladerSkallJagHa(10, true); // "Regnjacka.."
vilkaKladerSkallJagHa(-10, false); // "Vinterjackan.."
```

Notera användandet av `!` när vi kollar om det regnar.

### Att skicka tillbaka någonting från en funktion
Vi har tidigare tittat på funktioner som skriver ut någonting när vi anropar dem, men vore det inte bra om vi istället för att lägga in `console.log` i våra funktioner istället kunde skicka tillbaka ett värde som vi kunde spara undan? Låt oss säga att vi har två mätare. En som berättar hur temperaturen är och en annan som berättar om det regnar.

```javascript

const hamtaTemperatur = function() {
    return 7;
}

const hamtaRegnarDet = function() {
    return true;
}

```
Vi skapar upp funktionerna precis som tidigare, men istället för att skriva ut någonting så använder vi oss av nyckelordet `return`. När vi når raden med return så skickas värdet tillbaka till platsen som anropade funktionen, vilket gör att vi kan skriva följande program.
```javascript

const hamtaTemperatur = function() {
    return 7;
}

const hamtaRegnarDet = function() {
    return true;
}

const temperaturen = hamtaTemperatur(); // temperaturen innehåller nu 7
const regnarDet = hamtaRegnarDet(); // regnarDet innehåller nu true

console.log("Temperaturen är " + temperaturen);
if(regnarDet) {
    console.log("Det regnar");
} else {
    console.log("Det regnar inte");
}


```
Vi har nu skapat ett program som hämtar ut två väden ur två funktioner och sparar dessa i två konstanter. 

### Funktioner och sidoeffekter
När man pratar om funktioner så brukar man även prata om sidoeffekter. Enkeltförklarat så har en funktion en sidoeffekt om den påverkar någonting på "utsidan", låt oss titta på ett exempel.
```javascript

const funktionMedSidoEffekt = function() {
    console.log("Hej, jag är en sidoeffekt");
    return 14;
}

```
Här är console.log en sidoeffekt i funktionen då vi anropar den och den ger en påverkan utanför funktionen genom att skriva ut text. Om vi inte hade haft console.log med i funktionen hade den varit utan sidoeffekter. Sidoeffekter kan även se ut på andra sätt, som exemplet nedan.
```javascript

let temperaturen = 12;

const funktionMedSidoEffekt = function() {
    temperaturen = 33;
}

funktionMedSidoEffekt();

console.log(temperaturen); // Skriver ut 33

```

Här är påverkar funktionen variabeln `temperaturen` vilket gör att den är förändrad efter det att vi anropat vår funktion. Rent allmänt så brukar man kalla funktioner utan sidoeffekter för `pure functions`.

## Arrayer börjar på [0]

Ibland behöver man spara undan en lista med saker. Saker i det här fallet kan vara allt från siffror och text till mer komplexa objekt. Inom programmering kallas en lista för en "array". Låt oss skapa en array som innehåller tre ord och skriva ut det första och sista värdet ur den.

```javascript
const ordlista = ["fot", "hand", "ben"];
console.log(ordlista.length); // skriver ut längden, vilket är 3
console.log(ordlista[0]); // "fot"
console.log(ordlista[ordlista.length - 1]); // "ben"
```

För att skapa listan har vi använt `[` och `]`, för att sedan lista våra värden efter varandra. För att ta reda på en listas längd använde vi `ordlista.length`. När vi sedan vill hämta ett värde ur vår array använder vi hakaparenteser för att komma åt dessa. Notera att _den första platsen i en array har ett index som är 0_ och den sista platsen i en array har ett index som är listans längd - 1, vilket skrivs som `ordlista[ordlista.length-1]`.

På samma sätt som listan med ord kan vi skapa en lista som innehåller tal.

```javascript
const nummerlista = [34, 43, 234, 95];
console.log(nummerlista.length); // 4
console.log(nummerlista[0]); // 34
console.log(nummerlista[nummerlista.length - 1]); // 95
```

### Att lägga till och ta bort ur en array

Vi har sett hur man skapar upp en array och hur vi kan hämta ut värden ur olika platser ur arrayen. Men hur gör vi om vi vil lägga till eller ta bort någonting?

```javascript
const nummerlista = [34, 43, 234, 95];
console.log(nummerlista.length); // 4

nummerlista.push(999); // Lägger till 999 i slutet
console.log(nummerlista.length); // 5

const varde = nummerlista.pop(); // Hämtar ut det sista värdet
console.log(nummerlista.length); // 4
console.log(varde); // 999
```

Här använder vi oss av två metoder som finns i array objektet, nämligen `push` och `pop`. Push lägger till ett värde i slutet av en array och pop hämtar det sista värdet och tar bort det ur arrayen.

### Arrayer och loopar

En vanlig sak man behöver göra är att loopa igenom en array. Det kan t.ex. vara att man skall summera alla tal i en lista eller kanske lista alla namn i ett adressregister. Låt oss skapa en lista med tal och loopa igenom den.

```javascript
const kostnader = [34, 43, 234, 95];

for (var i = 0; i < kostnader.length; i++) {
  console.log(kostnader[i]);
}
```

Vi börjar med att använda nyckelordet `for` för att berätta att vi kommer att skapa en for-loop. Vi går sedan vidare och berättar att vi vill att vår loop skall börja på noll och fortsätta så länge `i < kostnader.length`, vilket betyder att vi vill fortsätta tills dess att i nått listans slut.

För att göra vårt program lite nyttigare kan vi försöka ta reda på summan av alla våra tal i vår lista med kostnader.

```javascript
const kostnader = [34, 43, 234, 95];
let summa = 0; // Skall vara utanför vår loop

for (var i = 0; i < kostnader.length; i++) {
  summa = summa + kostnader[i];
}

console.log(summa); // 406
```

För att beräkna summan i vår lista medkostnader så behöver vi skapa en variabel som innehåller vår summa. Vi skapar därför en variabel som heter summa och som har ett startvärde på noll. Notera att summan skapas utanför vår for loop för att vi inte skall sätta den till noll varje varv.

Vi går sedan vidare och loopar igenom våra kostnader och lägger till kostnaden till summa genom att skriva `summa = summa + kostnader[i]`.

### If-satser och continue

I ett sista (näst sista..) exempel kan vi titta på hur vi kan använda if-satser för att beräkna summan av alla tal som är större än 50.

```javascript
const kostnader = [34, 43, 234, 95];
let summa = 0;

for (var i = 0; i < kostnader.length; i++) {
  if (kostnader[i] > 50) {
    summa = summa + kostnader[i];
  }
}

console.log(summa); // 329
```

Detta är ett helt korrekt program som ger ett korrekt resultat. Om vi skall omvandla programmet till ren svenska så skulle vi kunna beskriva det som _"loopa igenom alla kostnader och om kostnaden är mer än femtio så lägg till det till summan"_. Ett annat sätt att beskriva ett liknande program skulle vara att säga _"loopa igenom och summera alla kostnader men hoppa över alla summor som inte är mer än femtio kronor"_.

```javascript
const kostnader = [34, 43, 234, 95];
let summa = 0;

for (var i = 0; i < kostnader.length; i++) {
  if (kostnader[i] <= 50) {
    continue; // Går vidare till nästa varv
  }

  summa = summa + kostnader[i];
}

console.log(summa); // 329
```

Här introducerar vi ett nytt nyckelord som kallas för `continue`. När nyckelordet körs så hoppar vi över allting som kommer senare i vår for-loop och börjar om på nästa varv direkt.

### If-satser och break

Låt oss säga att vi skall loopa igenom en miljon rader i ett försök att hitta ett visst värde, t.ex. ett telefonnummer. Ett naivt sätt att göra det på skulle vara att göra följande.

```javascript
let storTelefonLista = // en enorm lista på en miljon rader, minst
let telefonnummer = 07012345;

for (var i = 0; i < storTelefonLista.length; i++) {
  if (storTelefonLista[i] === telefonnummer) {
    console.log("Hittat!");
  }
}
```

Problemet är att om vi skulle hitta vårt telefonnummer på plats 10 i vår stora lista så skulle vi ändå behöva gå igenom resten av listan, och sånt tar tid. Vi kan dock rädda situationen med nyckelordet `break`.

```javascript
let storTelefonLista = // en enorm lista på en miljon rader, minst
let telefonnummer = 07012345;

for (var i = 0; i < storTelefonLista.length; i++) {
  if (storTelefonLista[i] === telefonnummer) {
    console.log("Hittat!");
    break; // Hoppar ur for-loopen
  }
}
```

När `break` körs så kommer vi att hoppa ur vår for-loop och därmed slippa kolla alla andra rader när vi redan hittat vårt telefonnummer.

## Objekt

Vi har tidigar skapat variabler och konstanter som innehåller tal, text samt true och false. Men hur skall man göra om man vill representera en person som har ett namn och en ålder? Vi skulle såklart kunna använda oss av mer än en variabel, men det blir snabbt ganska stökigt - speciellt om vi t.ex. vill skapa en lista som innehåller namn och ålder på alla våra kompisar. Vilken tur för oss att objekt kommer till vår räddning!

För att skapa ett kompis-objekt i sin enklaste form så gör man så här.

```javascript
let kompis = { namn: "Nils", alder: 41 };
console.log(kompis.namn); // "Nils"
console.log(kompis.alder); // 41
```

Vi börjar genom att skapa en helt vanlig variabel genom att använda `let` och sedan ge variabeln ett passande namn. Vi använder sedan så kallade "måsvingar" för att berätta att det är just ett objekt vi vill skapa. Ett objekt med enbart `{}` är dock inget mer än en tom behållare, så för att lägga till en namn-egenskap så skriver vi in `namn: "Nils"`. Vi avslutar sedan vårt objekt med att på samma sätt lägga till ålder.

Vi har nu skapat ett objekt som heter `kompis`. För att komma åt vårt objekts egenskaper använder vi oss av `objektnamn.egenskap`, vilket i det här fallet blir `kompis.namn`.

### Metoder och funktioner

Ibland räcker det inte med att enbart ha egenskaper av typen text och nummer i sitt objekt. Vore det inte skönt om vårt kompisobjekt kunde skriva ut lite information om sig själv?

```javascript
let kompis = { namn: "Nils", alder: 41 };
kompis.skrivut = function() {
  console.log("Jag heter" + this.namn + " och är " + this.alder + " år gammal");
};

kompis.skrivut(); // "Jag heter Nils och är 41 år gammal"
```

Vi börjar med att skapa upp vår kompis, men vi lägger sedan till en funktion på vår kompis genom att skriva `kompis.skrivut = function`. Det här är alltså en helt vanlig funktion som vi kopplar till en egenskap i vårt kompisobjekt. När vi sedan vill skriva ut namn och ålder så använder vi oss av `this` för att berätta att det är egenskaper som bor på samma plats som funktionen vi vill nå. Om en funktion sitter fast i ett objekt på det här sättet brukar det kallas för `metod`, men den fungerar på samma sätt som en funktion.

Vi kan även skriva det hela lite mer kompakt genom att skapa vår funktion när vi skapar vårt objekt.

```javascript
let kompis = {
  namn: "Nils",
  alder: 41,
  skrivut: function() {
    console.log(
      "Jag heter" + this.namn + " och är " + this.alder + " år gammal"
    );
  }
};

kompis.skrivut(); // "Jag heter Nils och är 41 år gammal"
```

## DOM och DOM manipulation

### Hur man hämtar ut element ur DOM
För att hämta ut element ur DOM med hjälp av javascript så kan man använda sig av querySelector samt querySelectorAll. Det två metoderna tar emot en css selektor och returnerar antingen ett element eller flera, beroende på vilken metod du använt.

```javascript
// Hämtar ut det första p elementet den hittar
let forstaParagrafen = document.querySelector("p");

// Hämtar ut alla p element den hittar
let allaParagrafer = document.querySelectorAll("p");
```

Du kan även använda dig av mer avancerade selektorer för att hämta ut element.
```javascript

let forstaParagrafenIArticle = document.querySelector("article p");
```

### Hur man lägger till en klass
För att lägga till en klass på ett element så behöver du först hämta ut elementet med en av metoderna vi disukterade tidigare för att sedan använda dig av classList.add() för att lägga till den nya klassen.

```javascript
let forstaParagrafen = document.querySelector("p");

// Lägger till en klass på den första paragrafen
forstaParagrafen.classList.add("minklass");
```

För att lägga till en klass på alla element så behöver vi använda oss av loop.

```javascript
let allaParagrafer = document.querySelectorAll("p");

for(var i = 0; i < allaParagrafer.length; i++) {
    allaParagrafer[i].classList.add("minklass");
}
```

### Hur man ändrar style på ett element
```javascript
let forstaParagrafen = document.querySelector("p");

// Lägger till en style på color, notera att man inte skriver background-color utan att det skrivs ihop.
forstaParagrafen.style.backgroundColor = "blue";
```

### Hur man läser ut och sätter ett attribut
För att läsa och sätta attribut på ett element måste vi först hämta ut elementet och sedan använda oss av getAttribute("attributnamn") eller setAttribute("namn", "värde") för att läsa och skriva attribut till elementet.

```javascript
let forstaParagrafen = document.querySelector("p");

// Hämtar ut värdet på attributet name och sparar i en konstant
const paragrafName = forstaParagrafen.getAttribute("name");

// Hämtar ut det första a elementet den hittar och sätter namnet til samma namn som din paragraf
let forstaElementet = document.querySelector("a");
forstaElementet.setAttribute("name", paragrafName);
```

### Att skapa element med javascript
Förutom att hämta och manipulera element så kan du äen skapa nya. Detta gör du genom att använda metoden createElement.

```javascript
var nyParagraf = document.createElement("p");
```

För att få din nya paragraf att synas så behöver du stoppa in den i ett redan tidigare skapat element. Enklast är att hämtas ut föräldern i vilken du vill stoppa in det nya elementet och lägga till det med appendChild tbd...

```javascript
let forstaArticle = document.querySelector("article");

var nyParagraf = document.createElement("p");

// Lägger till din nya paragraf sist bland alla paragrafer i din article
forstaArticle.appendChild(nyParagraf);

```
Du kan även lägga till attribut och klasser på elementet innan du lägger in det.

```javascript
let forstaArticle = document.querySelector("article");

var nyParagraf = document.createElement("p");
nyParagraf.setAttribute("namn","jag heter paragraf");
nyParagraf.classList.add("klassen");

forstaArticle.appendChild(nyParagraf);
```


